from distutils.core import setup, Extension
from Cython.Build  import cythonize
import numpy


setup(
    name = "test_class",
    ext_modules = cythonize(
        [
            Extension("c_funcs",
                      sources=["src/MIMGP_funcs.pyx"],
                      include_dirs=[numpy.get_include()],
                      language = "c++",
                      extra_compile_args=["-std=c++14", "-O3", "-Wno-unused-function"]
            )
        ],
        force = True,
        compiler_directives={
            "language_level": 3,
        }
    )
)
