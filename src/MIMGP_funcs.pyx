# coding: utf8
# cython: boundscheck= False
# cython: wraparound = False
import cython

import numpy as np

from libc.stdlib cimport malloc, free
from cpython.mem cimport PyMem_Malloc, PyMem_Free
from libcpp cimport bool
from libc.math cimport sin, cos, exp, log, M_PI, NAN
from libc.math cimport pow as cpow

from cython.parallel import prange
from scipy.linalg.cython_blas cimport dgemm
from scipy.linalg.cython_lapack cimport dpotrf, dpotrs, dgesv, dgesvx, dgelsd

import warnings




# Bases
cpdef double[:] fourier_basis_functions(float t, int n_bases, float tau):
    """
    Input : t      , float, instant of observation
            n_bases, int, number of basis function
            tau    , float, observed period
    Output: array[double], fourier basis function
    """
    cdef:
        int n_functions, j
        double[:] b = np.empty((n_bases))

    # n_functions = int((n_bases-1)/2)
    n_functions = int((n_bases-2)/2)
    
    b[0] = 1
    for j in range(n_functions):
        b[j + 1] = cos(2*M_PI*(j + 1)*t/tau)
        b[n_functions+j+1] = sin(2*M_PI*(j + 1)*t/tau)
        
    return b

cpdef double[:] exp_basis_functions(float t, int n_bases, float tau):
    """
    Input : t      , float, instant of observation
            n_bases, int, number of basis function
            tau    , float, observed period
    Output: array[double], exp basis function
    """
    cdef:
        int j
        float space_between_exp, base_center, gamma
        double s
        double[:] b = np.empty((n_bases))
    
    space_between_exp = tau/(n_bases-1)
    gamma = cpow(space_between_exp, 2)
    
    b[0] = exp(-0.5*cpow(t, 2)/gamma)
    b[n_bases - 1] = exp(-0.5*cpow(tau-t, 2)/gamma)
    s = b[0] + b[n_bases - 1]
    base_center = 0
    # Compute elements
    for j in range(n_bases - 2):
        base_center += space_between_exp
        b[j + 1] = exp(-0.5*cpow(base_center - t, 2)/gamma)
        s += b[j + 1]
    # normalize
    for j in range(n_bases):
        b[j] /= s
        
    return b


cpdef double[:] c_local_exp_basis_functions(float t, int n_bases,
                                          float tau, double[:] centers):
    """
    Input : t      , float, instant of observation
            n_bases, int, number of basis function
            tau    , float, observed period
            centers, float MemoryView, center of gaussians
    Output: array[double], exp basis function
    """
    cdef:
        int j
        float space_between_exp, base_center, gamma
        double s
        double[:] b = np.empty((n_bases))

    space_between_exp = tau/(n_bases-1)
    gamma = cpow(space_between_exp, 2)
        
    b[0] = exp(-0.5*cpow(t, 2)/cpow(centers[0], 2))
    b[n_bases - 1] = exp(-0.5*cpow(tau-t, 2)/cpow(centers[n_bases-1], 2))
    s = b[0] + b[n_bases - 1]
    base_center = 0
    # Compute elements
    for j in range(1, centers.shape[0] - 1):
        gamma = cpow(max(centers[j] - centers[j-1] , centers[j+1] - centers[j]), 2)
        b[j] = exp(-0.5*cpow(centers[j] - t, 2)/gamma)
        s += b[j]
    # normalize
    for j in range(n_bases):
        b[j] /= s
        
    return b

# Compute Probas for a set of samples
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double[:, :] c_probas(const double[:, :] X_flatten,
                            const double[:] t_flatten,
                            const unsigned long[:] starts,
                            const double[::1, :, :, :] K,
                            const int[:] t_indexes_flatten,
                            const double[:, :] B,
                            double[:, :, :] alpha,
                            double[:] weights_c,
                            int nb, int nc, double epsilon = 1e-12):
    cdef:
        int i, j, k, b, c, n_samples
        int ti, n_basis = alpha.shape[2]
        int unit_ = 1
        double alpha_ = 1., neg_alpha_ = -1., zero_ = 0.
        double xT_K_inv_x, sum_log_diag_L, prior_max, sum_exp, LSE
        int[:] tmp_ind
        double[:] t_, X_, Ki_inv_dot_xc
        double[:, :] Ki, Li
        double[:, :] probas
        # Fortran order variables
        double[::1, :] Bi

    n_samples = starts.shape[0] - 1
    probas = np.empty((n_samples, nc))

    for i in range(n_samples):
        # Initialize probas
        for c in range(nc):
            probas[i, c] = log(weights_c[c])

        ti = starts[i+1] - starts[i]
        t_ = np.copy(t_flatten[starts[i]:starts[i+1]])
        tmp_ind = np.copy(t_indexes_flatten[starts[i]:starts[i+1]])

        Ki_inv_dot_xc = np.empty((ti))
        Ki = np.empty((ti, ti))
        Li = np.empty((ti, ti))
        
        Bi = np.empty((ti, n_basis), order = 'F')
        for j in range(ti):
            for k in range(n_basis):
                Bi[j, k] = B[tmp_ind[j], k]

        X_ = np.empty((ti))

        for b in prange(nb, nogil = True, schedule = 'static', chunksize = 1):            
            for c in prange(nc):
                X_[...] = X_flatten[starts[i]:starts[i+1], b]
                
                for j in range(ti):
                    for k in range(j, ti):
                        Ki[j, k] = K[tmp_ind[j], tmp_ind[k], c, b]
                        if k == j:
                            Ki[j, j] += epsilon
                        else:
                            Ki[k, j] = Ki[j, k]
                            
                # Center X_
                dgemm('N', 'N', &ti, &unit_, &n_basis, &neg_alpha_,
                      &Bi[0, 0], &ti, &alpha[c, b, 0], &n_basis, &alpha_, &X_[0], &ti)
                # Solve linear problem: Ki_inv_dot_Xc = Sig^-1*(x - mu)
                chol_c(Ki, Li, ti)
                cho_solve_vec_c(Li, X_, Ki_inv_dot_xc, ti, 1)
                # xT_K_inv_x = Xc.T * Ki_inv_dot_xc
                dgemm('T', 'N', &unit_, &unit_, &ti, &alpha_,
                      &X_[0], &ti, &Ki_inv_dot_xc[0], &ti, &zero_, &xT_K_inv_x, &unit_)
                # Compute log det of Cholesky decomposition
                sum_log_diag_L = 0
                for j in range(ti):
                    sum_log_diag_L += log(Li[j, j])

                # log_likelihood + log_prior #{Zi = c} * P(y | Z = c)
                probas[i, c] -= (0.5 * xT_K_inv_x + sum_log_diag_L)
                
        # Scale for exp
        prior_max = max(probas[i, :])
        # LogSumExp trick for denominator
        sum_exp = 0
        for c in range(nc):
            sum_exp += exp(probas[i, c] - prior_max)
        LSE = prior_max + log(sum_exp)
        # compute posterior: #{Zi = c} * P(y | Z = c) / sum_i(P(y | Z = i))
        for c in range(nc):
            probas[i, c] = np.exp(probas[i, c] - LSE)
                    
    return probas

# Compute Log-Marginal Likelihood for a fixed band / class
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double[:] c_lml(const double[:] theta, const double[:, :] K,
                      const double[:] T, const double[:, :] B,
                      const double[:] X_c_flatten, const double[:] t_c_flatten,
                      const int[:] t_indexes_flatten,
                      const unsigned long[:] starts,
                      int n_basis, bool eval_gradient = False,
                      const double[:,:,:] K_gradient = None,
                      double epsilon = 1e-12):
    cdef:
        int i, k, n_samples, theta_size
        int[:] tmp_ind_
        double[:] log_likelihood_ = np.zeros([theta.size + 1])
        double[:] log_likelihood_int = np.empty([theta.size + 1])
        # Fortran order variables
        double[::1] alpha = np.empty((n_basis), order = 'F')

    # Initialization
    n_samples = starts.shape[0] - 1
    theta_size = theta.size

    # First step: Compute alpha
    alpha = c_compute_alpha(K, T, B, X_c_flatten, t_c_flatten,
                            t_indexes_flatten, starts,
                            n_basis, epsilon)
    
    # Second step: Compute LML and gradient
    for i in range(n_samples):
        tmp_ind_ = t_indexes_flatten[starts[i]:starts[i+1]].copy()
        # Compute likelihood for sample i
        log_likelihood_int = compute_loglike_i(
            X_c = X_c_flatten[starts[i]:starts[i+1]].copy(),
            t_c = t_c_flatten[starts[i]:starts[i+1]].copy(),
            tmp_ind = tmp_ind_, theta_size = theta_size,
            alpha_ = alpha.copy(), Bi = np.asfortranarray(B.base[tmp_ind_, :]),
            Ki = K.base[np.ix_(tmp_ind_, tmp_ind_)],
            Ki_grad = np.asfortranarray(K_gradient.base[np.ix_(tmp_ind_, tmp_ind_, )]),
            n_basis_ = n_basis, ti_ = starts[i+1] - starts[i], epsilon_ = epsilon,
            eval_gradient_ = eval_gradient)
        # Add values to log-likelihood
        for k in range(theta_size + 1):
            log_likelihood_[k] += log_likelihood_int[k]

    return log_likelihood_

# Sub routines - Compute LML
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double[:] compute_loglike_i(double[:] X_c, double[:] t_c,
                        int[:] tmp_ind, int theta_size,
                        double[::1] alpha_, double[::1, :] Bi,
                        double[:, :] Ki, double[::1, :, :] Ki_grad,
                        int n_basis_, int ti_, double epsilon_,
                        bool eval_gradient_ = False):
    cdef:
        int j, k, theta_ind, unit_ = 1
        double dunit_ = 1., neg_alpha__ = -1., beta_ = 1., zero_ = 0.
        double sum_log_diag_L, xT_K_inv_x, tmp
        double[:] Ki_inv_dot_xc = np.empty((ti_))
        double[:] log_likelihood_ = np.zeros(([theta_size + 1]))
        double[:, :] Li = np.empty((ti_, ti_))
        double[::1, :] Ki_inv = np.empty((ti_, ti_), order = 'F')
        # double[:] tmp_res = np.empty((ti_))
        double *tmp_res = <double *> malloc(ti_ * sizeof(double))

    # Center X in X_c
    dgemm('N', 'N', &ti_, &unit_, &n_basis_, &neg_alpha__,
          &Bi[0, 0], &ti_, &alpha_[0], &n_basis_, &dunit_, &X_c[0], &ti_)
    
    for j in range(ti_):
        Ki[j, j] += epsilon_
    # Cholesky solve
    chol_c(Ki, Li, ti_)
    cho_solve_vec_c(Li, X_c, Ki_inv_dot_xc, ti_, 1)
    
    xT_K_inv_x = 0
    sum_log_diag_L = 0
    # xT_K_inv_x = Xc.T * Ki_inv_dot_xc
    dgemm('T', 'N', &unit_, &unit_, &ti_, &dunit_,
          &X_c[0], &ti_, &Ki_inv_dot_xc[0], &ti_, &zero_, &xT_K_inv_x, &unit_)
    # Compute log det of Cholesky decomposition
    for j in range(ti_):
        sum_log_diag_L += log(Li[j, j])
        
    # Update likelihood
    log_likelihood_[0] = - ( 2 * sum_log_diag_L + xT_K_inv_x + ti_ * log(2 * M_PI) )
    
    if eval_gradient_ == True:
        # Compute Ki_inv
        cho_solve_mat_c(Li, np.eye(Li.shape[0], order = 'F'), Ki_inv,
                        ti_, ti_)
        for theta_ind in range(theta_size):
            for j in range(ti_):
                tmp = 0
                for k in range(ti_):
                    # Trace(Ki^{-1} * Ki_grad)
                    log_likelihood_[theta_ind+1] -= (Ki_inv[j,k] * Ki_grad[k, j, theta_ind])
                    # Compute Trace(beta.T Ki_grad beta)
                    tmp += (Ki_inv_dot_xc[k] * Ki_grad[k, j, theta_ind])
                log_likelihood_[theta_ind+1] += (Ki_inv_dot_xc[j] * tmp)
                
    free(<void *> tmp_res)
    return log_likelihood_

# Subroutine Compute alpha for a fixed band / class
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef double[::1] c_compute_alpha(const double[:, :] K,
                                  const double[:] T, const double[:, :] B,
                                  const double[:] X_c_flatten, const double[:] t_c_flatten,
                                  const int[:] t_indexes_flatten,
                                  const unsigned long[:] starts,
                                  int n_basis, double epsilon = 1e-12):
    cdef:
        int i, j, k, n_samples, ti
        int info, rank_, lwork, unit_ = 1
        int len_t = T.shape[0], nlvl = max(0, int(log(n_basis/26)/log(2) + 1))
        double rcond = -1., alpha_ = 1., neg_alpha_ = -1., beta_ = 1., zero_ = 0., wkopt
        double[:] t_c, X_c
        int[:] tmp_ind
        double[:, :] Ki, Li
        # Fortran order variables
        double[::1, :] Mi, Bi
        double[::1, :] G = np.zeros((n_basis, n_basis), order = 'F')
        double[::1, :] G_copy = np.zeros((n_basis, n_basis), order = 'F')
        double[::1] D = np.zeros((n_basis), order = 'F')
        double[::1] alpha = np.empty((n_basis), order = 'F')
        # To solve linear equations
        double[::1] s_ = np.empty((n_basis))
        double *work
        double *work_corr
        int *iwork

    # Initialization
    n_samples = starts.shape[0] - 1
    
    for i in range(n_samples):
        ti = starts[i+1] - starts[i]

        # Init views on the signal
        t_c = t_c_flatten[starts[i]:starts[i+1]].copy()
        X_c = X_c_flatten[starts[i]:starts[i+1]].copy()
        tmp_ind = t_indexes_flatten[starts[i]:starts[i+1]].copy()
        # Init Matrices
        Mi = np.empty((ti, n_basis), order = 'F')
        Li = np.empty((ti, ti))
        # Extract information
        Bi = np.asfortranarray(B.base[tmp_ind, :])
        Ki = K.base[np.ix_(tmp_ind, tmp_ind)]

        for j in range(ti):
            Ki[j, j] += epsilon
        
        # Solve linear problem
        chol_c(Ki, Li, ti)
        cho_solve_mat_c(Li, Bi, Mi, ti, n_basis)
        
        # G += Mi.T * Bi
        dgemm('T', 'N', &n_basis, &n_basis, &ti, &alpha_,
              &Mi[0,0], &ti, &Bi[0,0], &ti, &beta_, &G[0,0], &n_basis)
        # D += Mi.T * yi
        dgemm('T', 'N', &n_basis, &unit_, &ti, &alpha_,
              &Mi[0,0], &ti, &X_c[0], &ti, &beta_, &D[0], &n_basis)

    # Compute alpha - alpha = inv(G) * D
    alpha[...] = D; G_copy[...] = G
    solve_c(G_copy, D, alpha, n_basis, &info)
    # If inverse of G cannot be computed numerically, info = n_basis + 1
    if info > 0:
        alpha[...] = D; G_copy[...] = G
        # Initialize parameters - query the optimal workspace
        lwork = -1
        iwork = <int *> PyMem_Malloc((3*n_basis*nlvl + 11*n_basis) * sizeof(int))
        dgelsd(&n_basis, &n_basis, &unit_, &G_copy[0,0], &n_basis, &alpha[0], &n_basis,
               &s_[0], &rcond, &rank_, &wkopt, &lwork, iwork, &info)
        # Get parameters and compute least square ||G * alpha - D||^2
        lwork = int(wkopt)
        work = <double *> PyMem_Malloc(lwork * sizeof(double))
        dgelsd(&n_basis, &n_basis, &unit_, &G_copy[0,0], &n_basis, &alpha[0], &n_basis,
               &s_[0], &rcond, &rank_, work, &lwork, iwork, &info)
        PyMem_Free(work); PyMem_Free(iwork)
    
    return alpha


# Matrices operations
cdef void chol_c(double[:, :] A, double[:, :] B, int n) nogil:
    '''cholesky factorization of real symmetric positive definite float matrix A

    Parameters
    ----------
    A : memoryview (numpy array)
        n x n matrix to compute cholesky decomposition
    B : memoryview (numpy array)
        n x n matrix to use within function, will be modified
        in place to become cholesky decomposition of A. works
        similar to np.linalg.cholesky
    '''
    cdef int info
    cdef char* uplo = 'U'
    
    B[...] = A

    dpotrf(uplo, &n, &B[0,0], &n, &info)


cdef void cho_solve_vec_c(double[:, :] A, double[:] B, double[:] C,
                          int m, int n) nogil:
    '''cholesky factorization of real symmetric positive definite float matrix A

    Parameters
    ----------
    A : memoryview (numpy array)
        m x m lower triangular matrix L such as LL^T x = b.
    B : memoryview (numpy array)
        m x 1 Vector in Ax = b
    C : memoryview (numpy array)
        m x 1 Vector to use within function, will be modified
        in place to become cholesky decomposition of A. works
        similar to np.linalg.cho_solve
    '''
    cdef int info
    cdef char* uplo = 'U'
    
    C[...] = B

    dpotrs(uplo, &m, &n, &A[0,0], &m, &C[0], &m, &info)
    

cdef void cho_solve_mat_c(double[:, :] A, double[::1, :] B, double[::1, :] C,
                          int m, int n) nogil:
    '''cholesky factorization of real symmetric positive definite float matrix A

    Parameters
    ----------
    A : memoryview (numpy array)
        m x m lower triangular matrix L such as LL^T x = b.
    B : memoryview (numpy array, F contiguous)
        m x n Matrix in Ax = b
    C : memoryview (numpy array, F contiguous)
        m x n Matrix to use within function, will be modified
        in place to become cholesky decomposition of A. works
        similar to np.linalg.cho_solve
    '''
    cdef int info
    cdef char* uplo = 'U'

    C[...] = B

    dpotrs(uplo, &m, &n, &A[0,0], &m, &C[0, 0], &m, &info)

cdef void solve_c(double[::1, :] A, double[::1] B, double[::1] X,
                  int n_basis_, int* info) nogil:
    cdef :
        char equed = b'N'
        int unit_ = 1, i
        double rcond_s
        double ferr, berr
        int *ipiv_ = <int *> malloc(n_basis_ * sizeof(unsigned long))
        int *iwork_ = <int *> malloc(n_basis_ * sizeof(unsigned long))
        double *work_ = <double*> malloc(4 * n_basis_ * sizeof(double))
        double *r_ = <double*> malloc(n_basis_ * sizeof(double))
        double *c_ = <double*> malloc(n_basis_ * sizeof(double))
        double *Af = <double *> malloc(n_basis_ * n_basis_ * sizeof(double))
        
    X[...] = B

    dgesvx('E', 'N', &n_basis_, &unit_, &A[0,0], &n_basis_, Af, &n_basis_,
           &ipiv_[0], &equed, r_, c_, &B[0], &n_basis_, &X[0], &n_basis_, &rcond_s,
           &ferr, &berr, work_, &iwork_[0], info)

    free(<void*> ipiv_); free(<void*> iwork_)
    free(<void*> r_); free(<void*> c_)
    free(<void*> work_); free(<void*> Af)

