#!/bin/sh

rm -f c_funcs.so
python3 src/setup.py build_ext --inplace
if [ -f c_funcs.*.so ]; then
    mv c_funcs.*.so c_funcs.so
fi
