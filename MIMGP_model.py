# coding: utf8
import warnings
import os
import sys
import imp
from operator import itemgetter
import numpy as np
import scipy as sp
from scipy import interpolate as si

from scipy.linalg import cholesky, cho_factor, cho_solve, solve_triangular, solve
from scipy.linalg import LinAlgWarning, LinAlgError
from scipy.optimize import fmin_l_bfgs_b
#from scipy.special import softmax

from joblib import Parallel, delayed

from sklearn.base import BaseEstimator, clone
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_array

from sklearn import preprocessing

# Kernel depedency
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, WhiteKernel
from sklearn.exceptions import ConvergenceWarning
# Score metrics
from sklearn.metrics import accuracy_score


from c_funcs import *

# Workaround issue discovered in intel-openmp 2019.5:
# https://github.com/ContinuumIO/anaconda-issues/issues/11294
# os.environ.setdefault("KMP_INIT_AT_FORK", "FALSE")



def constrained_optimization(obj_func, initial_theta, bounds,
                             X, t, c, b, optimizer, in_memory, starts):
    if optimizer == "fmin_l_bfgs_b":
        theta_opt, func_min, convergence_dict = \
                            fmin_l_bfgs_b(obj_func, initial_theta,
                                          args=(True, X, t, c, b, in_memory, starts),
                                          bounds=bounds,
                                          maxls=20)
        if convergence_dict["warnflag"] != 0:
            warnings.warn("fmin_l_bfgs_b terminated abnormally with the "
                          " state: %s" % convergence_dict,
                          ConvergenceWarning)
    elif callable(optimizer):
        theta_opt, func_min = optimizer(obj_func, initial_theta,
                                        args=(True, X, t, c, b, in_memory, starts),
                                        bounds=bounds)
    else:
        raise ValueError("Unknown optimizer %s." % optimizer)

    return theta_opt, func_min

def flatten_list_of_lists(lst_of_lsts):
    N = sum(map(len, lst_of_lsts))  # number of elements in the flattened array   
    starts = np.empty(len(lst_of_lsts)+1, dtype=np.uint64)
    values = np.empty(N, dtype=np.double)

    starts[0], cnt = 0, 0
    for i,lst in enumerate(lst_of_lsts):
        for el in lst:
            values[cnt] = el
            cnt += 1       # update index in the flattened array for the next element
        starts[i+1] = cnt  # remember the start of the next list

    return starts, values


class MIMGP(BaseEstimator):
    """Irregularly Sampled Classification based on Gaussian Processes

    Parameters
    ----------
    kernel: kernel object, default is None
        The kernel specifying the covariance function of the Gaussian Process.

    tau:  int, optional (default: None)
        Highest value possible for time (used in the design matrix).
        Default (None) will be inferred from the data.

    n_basis_: int, optional (default: 10)
        size of alpha used for mean projection of the Gaussian Process,

    base: callable, optional (default: None)
        function used to build design matrix, 
        if None a Fourier Basis is used.

    optimizer: string or callable, optional (default: "fmin_l_bfgs_b")
        Can either be one of the internally supported optimizers for optimizing
        the kernel's parameters, specified by a string, or an externally
        defined optimizer passed as a callable. If a callable is passed, it
        must have the signature::
            def optimizer(obj_func, initial_theta, bounds):
                # * 'obj_func' is the objective function to be minimized, which
                #   takes the hyperparameters theta as parameter and an
                #   optional flag eval_gradient, which determines if the
                #   gradient is returned additionally to the function value
                # * 'initial_theta': the initial value for theta, which can be
                #   used by local optimizers
                # * 'bounds': the bounds on the values of theta
                ....
                # Returned are the best found hyperparameters theta and
                # the corresponding value of the target function.
                return theta_opt, func_min
        Per default, the 'fmin_l_bfgs_b' algorithm from scipy.optimize
        is used. If None is passed, the kernel's parameters are kept fixed.
        Available internal optimizers are::
            'fmin_l_bfgs_b'

    opt_gradient: string, 'complete' or 'alternate' (default: 'alternate')
        'complete' - Will compute the gradient descent for theta at once,
                     then compute the best mean's projection coefficients.
        'alternate'- Will compute the gradient descent by alternating theta
                     and mean's projection coefficients.

    n_restarts_optimizer: int, optional (default: 0)
        The number of restarts of the optimizer for finding the kernel's
        parameters which maximize the log-marginal likelihood. The first run
        of the optimizer is performed from the kernel's initial parameters,
        the remaining ones (if any) from thetas sampled log-uniform randomly
        from the space of allowed theta-values. If greater than 0, all bounds
        must be finite. Note that n_restarts_optimizer == 0 implies that one
        run is performed.

    epsilon: float or array-like, optional (default: 1e-10)
        Value added to the diagonal of the kernel matrix during fitting.
        Larger values correspond to increased noise level in the observations.
        This can also prevent a potential numerical issue during fitting, by
        ensuring that the calculated values form a positive definite matrix.
        If an array is passed, it must have the same number of entries as the
        data used for fitting and is used as datapoint-dependent noise level.
        Note that this is equivalent to adding a WhiteKernel with c=alpha.
        Allowing to specify the noise level directly as a parameter is mainly
        for convenience and for consistency with Ridge.

    copy_X_train: bool, optional (default: True), Probably to be deleted.

    random_state: int, RandomState instance or None, optional (default: None)
        The generator used to initialize the centers. If int, random_state is
        the seed used by the random number generator; If RandomState instance,
        random_state is the random number generator; If None, the random number
        generator is the RandomState instance used by `np.random`.
    rc
    Attributes
    ----------
    
    """
    def __init__(self, kernel=None, epsilon=1e-12,
                 base="fourier", n_basis_=10, tau=None,
                 optimizer="fmin_l_bfgs_b",
                 n_restarts_optimizer=0, copy_X_train=True,
                 random_state=None):
        # Time range (0, tau) for the time-series
        self.tau = tau

        # Kernel definition - sklearn function
        self.kernel = kernel
        self.epsilon = epsilon

        # Mean, design matrix definition
        self.alpha_ = None
        self.B = None
        self.dict_position = {}

        if callable(base):
            self.n_basis_ = n_basis_
            self.func_base = base
        elif base == "polynomial":
            self.n_basis_ = n_basis_
            self.func_base = lambda t_, J, tau_: np.array([np.power(t_ / tau_, j) for j in range(J)])
        elif base == "fourier":
            self.n_basis_ = n_basis_ * 2 + 1
            self.func_base = fourier_basis_functions
        elif base == "Bspline":
            self.n_basis_ = n_basis_+2
            self.func_base = self.bspline_basis_functions
        elif base == "local_exp":
            self.n_basis_ = n_basis_ + 2
            self.func_base = self.local_exp_basis_functions
        else:
            # Equidistant exponentials
            self.n_basis_ = n_basis_ + 2
            self.func_base = exp_basis_functions

        self.compute_alpha = True

        # Optimization parameters
        self.optimizer = optimizer
        self.n_restarts_optimizer = n_restarts_optimizer
        self.log_marginal_likelihood_value_ = 0

        # Data settings
        self.copy_X_train = copy_X_train
        self.random_state = random_state

    def fit(self, X, y, compute_likelihood=False):
        """
        Input : X, array-like, shape = (n_samples,
                                        1 + n_features, list of T_y instants)
                   the first feature is the observation instants of X
                y, array: class labels (transformed using preprocessing);
                   structured as [n samples]
        Output: self
        """
        # Init random state
        self._rng = check_random_state(self.random_state)

        # Prepare data: split sample time & features
        X_train_ = np.copy(X[:, 1:])
        self.n = X_train_.shape[0]
        self.n_bands_ = X_train_.shape[1]
        t_ = np.copy(X[:, 0])

        # Compute centers based on quantiles
        T = np.unique(np.hstack(t_))
        self.quantiles = np.quantile(np.hstack(t_).flatten(),
                                     np.linspace(0, 1, self.n_basis_),
                                     interpolation="nearest")
        
        # Init design matrix
        self.init_design_matrix(t_)

        # Preprocess classes
        le = preprocessing.LabelEncoder()
        y_train_ = le.fit_transform(y)
        self.classes_ = le.classes_
        self.n_classes_ = le.classes_.size

        # Compute weights
        self.index_C = [np.where(y_train_ == c)[0]
                        for c in range(self.n_classes_)]

        self.weights_ = np.array([float(ind_c.size)/self.n
                                  for ind_c in self.index_C])

        # Initialization alpha
        self.alpha_ = np.empty([self.n_classes_, self.n_bands_, self.n_basis_])

        # Kernels definition
        kernel_ = C(1.0, constant_value_bounds=(1e-5, 1e5)) \
            * RBF(1.0, length_scale_bounds=(1e-05, 1e5)) \
            + WhiteKernel(noise_level=0.1,
                          noise_level_bounds=(1e-10, 1e+1)) \
            if self.kernel is None else self.kernel

        self.kernels = {"{0}{1}".format(c, b): clone(kernel_)
                        for c in range(self.n_classes_)
                        for b in range(self.n_bands_)}

        if self.optimizer:
            # Objectif function for fixed c and b
            def obj_func(theta, eval_gradient=True, *args):
                if eval_gradient:
                    lml, grad = self.log_marginal_likelihood(theta,
                                                             eval_gradient,
                                                             args[0], args[1],
                                                             args[2], args[3],
                                                             args[4], args[5])
                    return -lml, -grad
                else:
                    return -self.log_marginal_likelihood(theta, eval_gradient,
                                                         args[0], args[1],
                                                         args[2], args[3],
                                                         args[4], args[5])

            # Optimization on c and b at once
            #   -   Last parameter is because the design matrix is in memory
            X_cb = np.array([[flatten_list_of_lists(X_train_[self.index_C[c], b]) \
                              for b in range(self.n_bands_)] \
                             for c in range(self.n_classes_)], dtype = object)
            t_c = np.array([flatten_list_of_lists(t_[self.index_C[c]]) \
                            for c in range(self.n_classes_)], dtype = object)
            in_memory_ = True
                        
            thetaCB_ = Parallel(n_jobs = -1)(
                delayed(constrained_optimization)(
                    obj_func,
                    self.kernels["{0}{1}".format(c, b)].theta,
                    self.kernels["{0}{1}".format(c, b)].bounds,
                    X_cb[c, b][1],
                    t_c[c][1],
                    c, b, self.optimizer,
                    in_memory_, X_cb[c, b][0])
                for c in range(self.n_classes_)
                for b in range(self.n_bands_))

            thetaCB = {"theta{0}{1}".format(c, b):
                       [thetaCB_.pop(0)]
                       for c in range(self.n_classes_)
                       for b in range(self.n_bands_)}

            # Serial implementation

            # thetaCB = {"theta{0}{1}".format(c, b):
            #            [(constrained_optimization(
            #                obj_func,
            #                self.kernels["{0}{1}".format(c, b)].theta,
            #                self.kernels["{0}{1}".format(c, b)].bounds,
            #                X_cb[c, b][1],
            #                t_c[c][1],
            #                c, b, self.optimizer,
            #                in_memory_, X_cb[c, b][0]))]
            #            for c in range(self.n_classes_)
            #            for b in range(self.n_bands_)}

            if (self.n_restarts_optimizer > 0):
                # Additional runs are performed from log-uniform chosen initial
                # theta bounds
                kernel_bounds = [self.kernels["{0}{1}".format(c, b)].bounds
                                 for c in range(self.n_classes_)
                                 for b in range(self.n_bands_)]

                if not np.isfinite(kernel_bounds).all():
                    raise ValueError("Multiple optimizer restarts"
                                     "(n_restarts_optimizer>0)"
                                     "requires that all bounds are finite.")

                for iteration in range(self.n_restarts_optimizer):
                    thetaCB_ = Parallel(n_jobs = -1)(
                        delayed(constrained_optimization)(
                            obj_func,
                            self._rng.uniform(
                                self.kernels["{0}{1}".format(c, b)].bounds[:, 0],
                                self.kernels["{0}{1}".format(c, b)].bounds[:, 1]),
                            self.kernels["{0}{1}".format(c, b)].bounds,
                            X_cb[c, b][1],
                            t_c[c][1],
                            c, b, self.optimizer,
                            in_memory_, X_cb[c, b][0])
                        for c in range(self.n_classes_)
                        for b in range(self.n_bands_))

                    for b in range(self.n_bands_):
                        for c in range(self.n_classes_):
                            thetaCB["theta{0}{1}".format(c, b)].append(thetaCB_.pop(0))
            
            # Select result from run with minimal (negative) log-marginal
            # likelihood
            self.log_marginal_likelihood_value_ = 0
            for c in range(self.n_classes_):
                for b in range(self.n_bands_):
                    lml_values_cb = list(map(itemgetter(1),
                                             thetaCB["theta{0}{1}".format(c, b)]))
                    i_min = np.argmin(lml_values_cb)
                    # Store theta
                    self.kernels["{0}{1}".format(c, b)].theta = \
                        thetaCB["theta{0}{1}".format(c, b)][i_min][0]
                    # Store lml
                    self.log_marginal_likelihood_value_ += \
                        thetaCB["theta{0}{1}".format(c, b)][i_min][1]

        # Compute alpha and likelihood for fixed or optimized theta
        if compute_likelihood:
            self.log_marginal_likelihood_value_ = 0
            
        for c in range(self.n_classes_):
            _, t_C = flatten_list_of_lists(t_[self.index_C[c]])
            T_, ind_ = np.unique(t_C, return_inverse = True)
            B_ = self.design_matrix(T_, in_memory=True)
            
            for b in range(self.n_bands_):
                starts_, X_train_C = flatten_list_of_lists(X_train_[self.index_C[c], b])
                K_ = self.kernels["{0}{1}".format(c, b)](
                    T_.reshape(-1, 1))
                # Compute alpha
                self.alpha_[c, b, :] = np.asarray(c_compute_alpha(
                    K = K_, T = T_, B = B_, X_c_flatten =  X_train_C,
                    t_c_flatten = t_C, t_indexes_flatten = ind_.astype(np.int32),
                    starts = starts_, n_basis = self.n_basis_, epsilon = self.epsilon))
                # Compute Likelihood
                if compute_likelihood:
                    self.log_marginal_likelihood_value_ += \
                                                           self.log_marginal_likelihood(
                                                               None,
                                                               None,
                                                               X_train_[self.index_C[c], :],
                                                               t_[self.index_C[c]],
                                                               c, b,
                                                               False)
        return self

    def predict(self, X):
        """Return classes estimates for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples)
            Returns the class for each sample in the model.
        """
        return self.classes_[np.argmax(self.predict_proba(X), axis=-1)]
 
    def predict_proba(self, X):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.
        """
        n_samples = X.shape[0]

        # Init design matrix
        self.init_design_matrix(X[:, 0])
        len_T = self.T.shape[0]
        K_ = np.empty((len_T, len_T, self.n_classes_, self.n_bands_), order = 'F')
        
        # Init C args
        starts_, t_ = flatten_list_of_lists(X[:, 0])
        X_ = np.empty((len(t_), X.shape[1] - 1))
        for b in range(self.n_bands_):
            _, X_[:, b] = flatten_list_of_lists(X[:, b+1])
            for c in range(self.n_classes_):
                kernel_ = self.kernels["{0}{1}".format(c, b)]
                K_[:, :, c, b] = kernel_(self.T.reshape([len_T, 1]))

        # Compute a posteriori probabilities
        probas = np.asarray(
            c_probas(X_flatten = X_,
                     t_flatten = t_, starts = starts_,
                     K = K_, t_indexes_flatten = self.ind_.astype(np.int32),
                     B = self.B, alpha = self.alpha_, weights_c = self.weights_,
                     nb = self.n_bands_, nc = self.n_classes_, epsilon = self.epsilon))

        return probas

    def score(self, X, y_true):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:      array-like, shape = (n_samples, 1 + n_features, T_y instants)
                the first feature is the observation instants of X
        y_true: array-like, shape = (n_samples, 1 class)
        Returns
        -------
        Score : float,
            Returns the model's accuracy.
        """
        return accuracy_score(y_true, self.predict(X))

    def input_missing_values(self, X, t, c = None, return_std = False, return_cov = False):
        """Return time-series values estimated for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        t: array_like, shape = (n(y) instants)
            contains the n(y) instants to input to signal X
        c:  int, default None
            class of the signal, if c = None: Applied Bayes decision rule
        return_std: bool, default False
            if True returns standard deviation of the signal to the mean.
        return_cov: bool, default False
            if True returns the covariance matrix associated to the process
        Returns
        -------
        mean_values: array-like, shape = (n(y), 1 + n_features)
            Returns the n(y) instants values for each feature.
        var: array-like float, shape = (n(y), n_features)
            if return_std, Variance evaluated on each point of the inputed time-series
        cov: Array-like, shape = (n_features, n(y), n(y))
            Covariance matrix associated to the inputed elements
        """
        if return_std and return_cov:
            raise RuntimeError("Not returning standard deviation of predictions when "
                               "returning full covariance.")
        t_ = check_array(t)
        
        x_ = X[1:]
        t_train = X[0].reshape([-1,1])
        

        if not hasattr(self, "kernels"): # Unfitted;no prediction
            raise RuntimeError("Model not fitted.")
        else:
            # Create predictive mean/var values and covariance matrix,
            #    mean values is containing times and values for each band
            mean_values = np.zeros([t_.shape[0], self.n_bands_ + 1])
            if return_cov:
                cov_mat = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])
            elif return_std:
                var_values = np.zeros([t_.shape[0], self.n_bands_])


            # Compute mean (and var, cov)
            if (c == None):
                # Prediction when class is unknown
                probas_c = self.predict_proba(np.array([X]))
                probas_c[0, probas_c[0,:] < 1e-20] = 0    # avoid approximation on zero machine

                if return_cov:
                    # Return mean and covariance matrix
                    expectations_squared_c = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])

                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_cov_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_cov=True)
                        # Save and ponderate them
                        mean_values += expectations_cov_c[0] * probas_c[0,ci]
                        # save covariance matrix for each class, weighted
                        cov_mat += expectations_cov_c[1] * probas_c[0,ci]

                        # compute correction term
                        for b in range(self.n_bands_):
                            expectations_squared_c[b]+= \
                                                np.dot(expectations_cov_c[0][:,(1+b):(2+b)],
                                                       expectations_cov_c[0][:,(1+b):(2+b)].T) \
                                                       * probas_c[0,ci]
                    
                    # correction terms in covariance
                    cov_mat += expectations_squared_c - \
                            np.matmul(mean_values[:,1:].reshape([self.n_bands_,t_.shape[0],1]),
                                      mean_values[:,1:].reshape([self.n_bands_,1,t_.shape[0]]))
                    
                    cov_mat_negative = cov_mat < 0
                    if np.any(cov_mat_negative):
                        warnings.warn("Corrected covariance matrix smaller than 0. "
                                      "Setting those values to 0.")
                        cov_mat[cov_mat_negative] = 0.0
                    
                elif return_std:
                    # Save expectations and std deviations for each classe, weighted by
                    #   its probability to belong to class c
                    expectations_squared_c = np.zeros([t_.shape[0],self.n_bands_])
                    
                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_std_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_std=True)
                        # Save and ponderate them
                        mean_values += expectations_std_c[0] * probas_c[0,ci]
                        expectations_squared_c += expectations_std_c[0][:,1:] \
                                                  * expectations_std_c[0][:,1:] * probas_c[0,ci]
                        # we use square since square root is the output
                        var_values += expectations_std_c[1]*expectations_std_c[1]*probas_c[0,ci]

                    # correction terms in variance
                    var_values += expectations_squared_c - mean_values[:,1:]*mean_values[:,1:]
                    
                    x_var_negative = var_values < 0
                    if np.any(x_var_negative):
                        warnings.warn("Corrected variances smaller than 0. "
                                      "Setting those variances to 0.")
                        var_values[x_var_negative] = 0.0

                # Case when return_cov and return_std are False
                else:
                    expectations_c = np.array([self.input_missing_values(X, t_,
                                                                         c = self.classes_[ci]) \
                                               *probas_c[0,ci]
                                               for ci in range(self.n_classes_)])
                    mean_values = expectations_c.sum(axis = 0)

            else:
                # Prediction when class is known
                mean_values[:, 0] = t_[:,0]               # Save temporal instants
                c_ = np.where(self.classes_ == c)[0][0]      # Uses labelEncoder defined in fit

                
                for b in range(self.n_bands_):
                    K = clone(self.kernels["{0}{1}".format(c_, b)])
                    Sigma = K(t_train.reshape([-1, 1]))
                    L, low = cho_factor(Sigma, lower=True)
                        
                    # Data - multi-dimentional centered output, y in formula
                    Bi = self.design_matrix(t_train.flatten())
                    mean = np.dot(Bi, self.alpha_[c_,b])
                    # Center X_train_
                    Xc = x_[b] - mean
                    if Xc.ndim == 1:
                        Xc = Xc[:, np.newaxis]
                        
                    K_inv_prod_Xc = cho_solve((L, low), Xc)
                    K_trans = K(t_, t_train)
                    
                    # Compute mean (right part)
                    x_mean = K_trans.dot(K_inv_prod_Xc)      # Line 4
                    # Compute mean on target points (B*alpha)
                    B = self.design_matrix(t_.flatten())
                    x_mean = np.dot(B, self.alpha_[c_, b]) + x_mean # undo normal
                    # update values for the band b
                    mean_values[:, b+1] = np.diag(x_mean)
                    
                    if return_cov:
                        # Compute K(t*) - K_trans (K^-1) K_trans.T
                        v = cho_solve((L, True), K_trans.T) # Line 5
                        cov_mat[b] = K(t_) - K_trans.dot(v)  # Line 6

                    elif return_std:
                        # compute inverse K_inv of K based on its Cholesky
                        # decomposition L and its inverse L_inv
                        L_inv = solve_triangular(L.T,
                                                 np.eye(L.shape[0]))
                        self._K_inv = L_inv.dot(L_inv.T)

                        # Compute variance of predictive distribution
                        var_values[:,b] = K.diag(t_)
                        var_values[:,b] -= np.einsum("ij,ij->i",
                                                     np.dot(K_trans, self._K_inv), K_trans)

                        # Check if any of the variances is negative because of
                        # numerical issues. If yes: set the variance to 0.
                        x_var_negative = var_values[:,b] < 0
                        if np.any(x_var_negative):
                            warnings.warn("Predicted variances smaller than 0. "
                                          "Setting those variances to 0.")
                            var_values[x_var_negative, b] = 0.0
                        
                      
        if return_cov:
            return mean_values, cov_mat
        if return_std:
            return mean_values, np.sqrt(var_values)
        return mean_values

    def log_marginal_likelihood(self, theta=None, eval_gradient=False,
                                X_train_C=None, t_C=None, c=None, b=None,
                                in_memory = False, starts=[None], compute_alpha=True):
        """Compute the LML, and the Gradient for a given class and band
        Parameters
        ----------
        theta      , list of theta (c and b)
        eval_gradient, evaluation of the gradient
        args    [0], X_train_C, array training samples for class c
                [1], t_C      , array of lists, observation instants
                [2], c, class considered
                [3], b, band to be optimized
        Returns
        -------
        LML, grad LML
        """
        
        # Check params
        if (theta is None) and (eval_gradient is True):
            raise ValueError(
                "Gradient can only be evaluated for theta!=None")
            return self.log_marginal_likelihood_value_

        # Transform signals to 1D array - Avoid to flatten during optimization
        if in_memory == False:
            starts, X_train_C = flatten_list_of_lists(X_train_C[:, b])
            _, t_C = flatten_list_of_lists(t_C)

        # Set Kernels
        if theta is None:
            kernel_ = self.kernels["{0}{1}".format(c, b)]
            theta = kernel_.theta
            eval_gradient = False
        else:
            kernel_ = self.kernels["{0}{1}".format(c, b)].clone_with_theta(theta)

        # Compute position of each temporal
        T_, ind_ = np.unique(t_C, return_inverse = True)
        # Precompute the whole basis
        B_ = self.design_matrix(T_, in_memory=in_memory)

        # Precompute the whole kernel at theta
        if eval_gradient:
            K_, K_gradient_ = kernel_(T_.reshape(-1, 1), eval_gradient=True)
        else:
            K_ = kernel_(T_.reshape(-1, 1))
            K_gradient_ = np.empty((T_.shape[0], T_.shape[0], theta.size))

        # Compute LML and Gradient w.r.t. theta
        lml_values_grad = np.asarray(
            c_lml(theta = theta, K = K_, T = T_, B = B_,
                  X_c_flatten = X_train_C, t_c_flatten= t_C,
                  t_indexes_flatten = ind_.astype(np.int32),
                  starts = starts, n_basis = self.n_basis_,
                  eval_gradient = eval_gradient, K_gradient = K_gradient_,
                  epsilon = self.epsilon))        

        # Return Log-Likelihood
        log_likelihood_ = lml_values_grad[0]
        if eval_gradient:
            log_likelihood_gradient_ = lml_values_grad[1:(theta.size + 1)]
            return log_likelihood_, log_likelihood_gradient_
        else:
            return log_likelihood_

    def init_design_matrix(self, t_):
        """ 
        Initialization of the design matrix and local "centers" if the basis is local
            Compute only once the whole matrix then for each call copy only
            the corresponding part.
        """
        self.T, self.ind_ = np.unique(np.hstack(t_),
                                      return_inverse = True)
        if self.tau is None:
            self.tau = int(self.T.max()) + 1      # Save maximum to define tau
        self.dict_position = {ti: i for i, ti in enumerate(self.T)}

        # Precompute the whole basis
        self.B = np.empty([self.T.size, self.n_basis_])

        for i, t in enumerate(self.T):
            # func_base should return all the basis for a given t
            self.B[i, :] = np.asarray(self.func_base(t, self.n_basis_, self.tau))
        

    def design_matrix(self, t, in_memory=False):
        """
        Matrix B in the paper.
        Conditions on band must be written here to design the model.

        Input : t, observation instants
        Output: B(t)
        """
        if in_memory is True:
            positions = [self.dict_position[t_] for t_ in t]
            B = self.B[positions, :]
        else:
            len_t = len(t)

            B = np.empty([len_t, self.n_basis_])
            for i, t_ in enumerate(t):
                B[i, :] = np.asarray(self.func_base(t_, self.n_basis_, self.tau))

        return B

    # Local basis functions (call c funcs and uses self to store "centers")
    def local_exp_basis_functions(self, t, J, tau):
        """
        Input : t,   float, instant of observation
                J,     int, number of basis function
                tau, float, observed period
        Output: array[float], local exp basis function
        """
        # Requires self to input centers computed previously
        b = c_local_exp_basis_functions(t, J, tau, self.quantiles.astype(float))
        
        return b

    # Python version of Bspline
    def bspline_basis_functions(self, t, n_bases, tau, degree=3, derivative=0):
        """
        Input : t,     float, instant of observation
            n_bases, int, number of basis function
            tau,   float, observed period
            degree,  int, degree of the spline interpolation
        Output: array[float], bspline basis function
        """
        # Auxillary function
        def Basis(b, t, tcl, derivative):
            vec = np.zeros_like(tcl[1])
            vec[b] = 1.0
            tcl = list(tcl)
            tcl[1] = vec.tolist()
            return si.splev(t, tcl, der=derivative)

        b = np.empty((n_bases,))
        tcl = si.splrep(self.quantiles, np.zeros_like(self.quantiles),
                        k=degree,
                        s=None,
                        per=False
        )

        for i, tbs_ in enumerate(self.quantiles):
            b[i] = Basis(i, t, tcl, derivative)

        return b/b.sum()
